import os
from flask import Flask, url_for

app = Flask(__name__)


@app.route("/")
def __init__():
    return f"""
        <h3>Image</h3>
        <img src="{url_for('static', filename='image.jpeg')}" class="image" />
        """


port = int(os.environ.get("PORT", 5000))
app.run(debug=True, host="0.0.0.0", port=port)
